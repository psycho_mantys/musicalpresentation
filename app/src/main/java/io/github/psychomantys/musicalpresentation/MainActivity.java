package io.github.psychomantys.musicalpresentation;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mMusic;
    ImageView mSlidesScreen;
    TextView mError;

    ObjectAnimator mFadeInAnimation,mFadeOutAnimation;
    Vector<Integer> mResIds;
    int currentSlide;
    int mSlideDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSlideDuration=getResources().getInteger(R.integer.slide_duration);
        currentSlide=0;

        mResIds=new Vector<>();
        mResIds=loadResource();


        mSlidesScreen=(ImageView) findViewById(R.id.iv_slide);
        mError=(TextView) findViewById(R.id.tv_error);

        mError.setVisibility(View.INVISIBLE);
        mError.setText( String.valueOf(mResIds.size()) );

        mFadeInAnimation=(ObjectAnimator)AnimatorInflater.loadAnimator(this, R.animator.fade_in);
        mFadeOutAnimation=(ObjectAnimator)AnimatorInflater.loadAnimator(this, R.animator.fade_out);

        mSlidesScreen.setImageResource(R.mipmap.ic_launcher);


        mFadeInAnimation.setTarget(mSlidesScreen);
        mFadeOutAnimation.setTarget(mSlidesScreen);

        mFadeOutAnimation.setStartDelay(mSlideDuration);

        mFadeInAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mError.setText("Mudou!! "+currentSlide);
                if( currentSlide<mResIds.size() ) {
                    mSlidesScreen.setImageResource(mResIds.elementAt(currentSlide));
                    currentSlide++;
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if( currentSlide<mResIds.size() ) {
                    mFadeOutAnimation.start();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

        });

        mFadeOutAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) {
                mFadeInAnimation.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });

        mFadeOutAnimation.start();

        mMusic=MediaPlayer.create(MainActivity.this, R.raw.sample);
        mMusic.start();
    }

    public Vector<Integer> loadResource() {

        Class<?> cls=R.drawable.class;
        R.drawable drawableResources=new R.drawable();
        final Field[] fields = cls.getDeclaredFields();
        Vector<Integer> ret=new Vector<>();

        for (Field field : fields) {
            final int resId;
            try {
                //field.
                String name=field.getName();
                if ( !name.startsWith("slide") ) {
                    continue;
                }
                resId = field.getInt(drawableResources);
            } catch (Exception e) {
                continue;
            }
            ret.add(resId);
        }
        return ret;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMusic.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMusic.start();
    }
}
